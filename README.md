# README for qith R package #
--------
***qith*** is a computional method to quantify intra-tumor heterogeneity and its spatial distribution using multi-region or single cell sequencing data.

### How to download HiCapp? ###
--------
Download the package using git clone or using the "download" link.

     git clone https://bitbucket.org/mthjwu/qith

### Software dependencies ###
--------
The package has been tested in R. 
The following R packages need user to install by themselves.

* vegan (https://cran.r-project.org/web/packages/vegan)

### input format ###
--------
* Molecular data matrix (such as gene expression, protein expression and mutation), with regional or single cell samples in columns and molecular features in rows.
* (optional) Spatial locations of regional or single cell samples, with regional or single cell samples in rows and X, Y, (Z) locations in columns.

### How to run qith ###
--------
* Load qith functions in R

        source("qith.R")

* Calculate the intra-tumor heterogeneity and its spatial distribution using example data

        source("run_lung_mass_spec.R")

* Check results

        head(qit)

### Who do I talk to? ###
--------
* Hua-Jun Wu (hjwu@jimmy.harvard.edu)
