#####
# Apply the qith function in lung mass spec data
#####


## load data
expr = read.delim("lung_mass_spec_expr.txt", row.names=1)
pheno = read.delim("lung_mass_spec_pheno.txt", row.names=1, stringsAsFactors=F)
pid = sort(unique(pheno$PatientID[grep("^P", pheno$PatientID)]))


## calculate the intra-tumor heterogeneity (ITH) and geographic diversification (GD)
qit = sapply(pid, function(x){
	s.pheno = pheno[pheno$PatientID==x & pheno$TumorOrNormal=="T",]
	s.expr = expr[,rownames(s.pheno)]
	s.geo = pheno[rownames(s.pheno), c("XLocation", "YLocation")]
	s.out = qith(s.expr, s.geo)
})
qit = data.frame(t(qit))


## rank normalize the ITH scores
qit$ITH.richness = rankNormVec(qit$ITH.richness)
qit$ITH.evenness = rankNormVec(qit$ITH.evenness)


